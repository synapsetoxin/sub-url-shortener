FROM python:3.11
WORKDIR /code
COPY requirements.txt /code/requirements.txt
COPY app /code/app
COPY migrations /code/migrations
COPY alembic.ini /code
COPY .env /code
RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt
EXPOSE 8000
CMD ["uvicorn", "app.main:app", "--proxy-headers", "--host", "0.0.0.0", "--port", "8000"]
