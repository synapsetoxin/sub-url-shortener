PYTHON = python3
APP = app.main:app
WORKER = uvicorn.workers.UvicornWorker
SERVER = uvicorn
WORKDIR = $(shell pwd)
PROD = no

all: venv install run

test: clean all

venv:
	$(PYTHON) -m venv venv

touch-venv:
	poetry shell
	#. $(WORKDIR)/venv/bin/activate

install: touch-venv
	poetry install

run:
	$(SERVER) --reload $(APP)

lint:
	flake8 .

clean:
	rm -rf venv .venv

celery:
	celery -A app.tasks worker --loglevel=info

alembic:
	alembic revision --autogenerate

build:
	docker compose up -d --build