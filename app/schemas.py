from datetime import datetime
from enum import Enum
from pydantic import BaseModel


class Role(Enum):
    ADMIN = "admin"
    WORKER = "worker"


class BaseSchema(BaseModel):
    class Config:
        extra = "ignore"
        str_to_lower = False
        from_attributes = True


class UrlSchema(BaseSchema):
    url: str
    code: str


class DomainSchema(BaseSchema):
    domain: str


class UserCreateSchema(BaseSchema):
    user_id: int


class UserSchema(BaseSchema):
    id: int
    user_id: int
    role: Role
    created_at: datetime
