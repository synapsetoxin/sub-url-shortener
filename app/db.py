from sqlalchemy.ext.asyncio import create_async_engine, async_sessionmaker
from sqlalchemy.orm import DeclarativeBase

from redis import Redis, ConnectionPool, StrictRedis

from app.config import settings


async_engine = create_async_engine(url=settings.DB_DSN, echo=False)
async_session_maker = async_sessionmaker(async_engine, expire_on_commit=False)


class Base(DeclarativeBase):
    pass


async def get_async_session():
    async with async_session_maker() as session:
        yield session


def get_cache() -> Redis:
    return StrictRedis(connection_pool=ConnectionPool(host='redis', port=6379, decode_responses=True))
