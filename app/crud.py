from typing import Any

from sqlalchemy import insert, select, delete, update
from sqlalchemy.ext.asyncio import AsyncSession

from app.models import UserOrm
from app.schemas import UserSchema, UserCreateSchema


async def create_user(session: AsyncSession, user: UserCreateSchema) -> UserSchema:
    stmt = insert(UserOrm).values(**user.model_dump()).returning(UserOrm)
    res = await session.execute(stmt)
    await session.commit()
    return res.scalar_one().to_read_model()


async def get_user(session: AsyncSession, user_id: int) -> UserSchema:
    stmt = select(UserOrm).filter_by(user_id=user_id)
    res = await session.execute(stmt)
    res = res.scalar_one().to_read_model()
    return res


async def update_user(
    session: AsyncSession, user_id: int, data: dict[str, Any]
) -> UserSchema:
    stmt = update(UserOrm).values(**data).filter_by(user_id=user_id).returning(UserOrm)
    res = await session.execute(stmt)
    await session.commit()
    return res.scalar_one().to_read_model()


async def delete_user(session: AsyncSession, user_id: int) -> UserSchema:
    stmt = delete(UserOrm).filter_by(user_id=user_id).returning(UserOrm)
    res = await session.execute(stmt)
    await session.commit()
    return res.scalar_one().to_read_model()
