from typing import Annotated
from datetime import timedelta

from fastapi import APIRouter, status, HTTPException, Depends
from fastapi_cache.decorator import cache
from sqlalchemy.exc import IntegrityError, NoResultFound
from sqlalchemy.ext.asyncio import AsyncSession
from redis import Redis

from app.schemas import UrlSchema, UserSchema, UserCreateSchema
from app.db import get_async_session, get_cache
from app.crud import create_user, get_user, delete_user

router = APIRouter(prefix="/api", tags=["API"])


@router.post("/shortify", status_code=status.HTTP_201_CREATED, response_model=UrlSchema)
async def create_url_endpoint(url: UrlSchema, r: Annotated[Redis, Depends(get_cache)]):
    r.set(url.code, url.url, ex=timedelta(days=1))  # noqa
    return url


@router.post("/user", status_code=status.HTTP_201_CREATED, response_model=UserSchema)
async def create_user_endpoint(
    user: UserCreateSchema, session: Annotated[AsyncSession, Depends(get_async_session)]
):
    try:
        return await create_user(session, user)
    except IntegrityError:
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST)


@router.get(
    "/user/{user_id}", status_code=status.HTTP_200_OK, response_model=UserSchema
)
@cache(expire=600)
async def read_user_endpoint(
    user_id: int, session: Annotated[AsyncSession, Depends(get_async_session)]
):
    try:
        return await get_user(session, user_id)
    except NoResultFound:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)


@router.delete("/user/{user_id}", status_code=status.HTTP_200_OK)
async def delete_user_endpoint(
    user_id: int, session: Annotated[AsyncSession, Depends(get_async_session)]
):
    try:
        await delete_user(session, user_id)
    except NoResultFound:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)
