import redis
import uvicorn
import logging
from fastapi import FastAPI, status, HTTPException, Depends, Request
from fastapi.responses import RedirectResponse
from fastapi_cache import FastAPICache
from redis import asyncio as aioredis
from fastapi_cache.backends.redis import RedisBackend
from typing import Annotated

from app.api import router
from app.db import get_cache
from app.config import settings

app = FastAPI()
app.include_router(router)


@app.get("/ping")
async def ping():
    return {"hello": "world"}


@app.get("/{code}", response_class=RedirectResponse)
async def index(code: str, request: Request, r: Annotated[redis.Redis, Depends(get_cache)]):
    coqwede = request.headers["Host"].split(".")[0]
    print(f'{coqwede=}')
    if url := r.get(code):
        return RedirectResponse(url=url)
    raise HTTPException(status_code=status.HTTP_404_NOT_FOUND)


@app.on_event("startup")
async def startup():
    FastAPICache.init(
        RedisBackend(aioredis.from_url(settings.REDIS_DSN)), prefix="fastapi-cache"
    )


if __name__ == "__main__":
    """
    Server configurations
    """
    uvicorn.run(
        app=f"app.main:app",
        reload=True,
        log_level=logging.INFO,
        use_colors=True,
    )
